import sys
import os
import logging
from contextlib import contextmanager

from configparser import ConfigParser, ExtendedInterpolation, InterpolationError
from pyutils.path import join_module_path

from pyutils.initialzation import main, parse_args
from pyutils.common import is_windows

from flask import Flask, request, abort
from flask_socketio import SocketIO
from pyutils.common import token
from functools import wraps
from http import HTTPStatus as status


class Config(ConfigParser):

    def __init__(self, config_path=None):

        super().__init__(interpolation=ExtendedInterpolation())

        self.config_path = (config_path or 
            os.environ.get("CONFIG_PATH", 
                join_module_path("config/localhost.cfg")))

        self.add_section("env")
        self.set("env", "module", os.path.dirname(__file__))
        self.set("env", "cwd", os.getcwd())

        for k, v in os.environ.items():
            try:
                self.set("env", k, v)
            except Exception:
                pass
            
        self.read(self.config_path)


    def get_section(self, section):

        ret = dict()

        if self.has_section(section):

            d = self._defaults.copy()
            value_getter = lambda option: self._interpolation.before_get(self,
                section, option, d[option], d)
            
            d.update(self._sections[section])

            for option in d.keys():
                try:
                    ret[option] = token(value_getter(option))
                except InterpolationError as e:
                    logging.warn(e)
        
        return ret



def sqla_to_object(sqla_obj):

    data = dict(sqla_obj.__dict__)
    data.pop("_sa_instance_state", None)

    return type("sqla_obj", (), data)


def route(rule, **options):

    def factory(func):
        setattr(func, "__route__", (rule, options))
        return func

    if callable(rule):
        func = rule
        rule = "/" + rule.__name__.lower()
        return factory(func)

    return factory


# def login_required(func=None, **options):

#     def factory(func):
#         setattr(func, "__auth__", options)
#         return func

#     if func is not None:
#         return factory(func)

#     return factory


def load_routes(obj, bind_obj=None):

    if not bind_obj:
        bind_obj = obj

    for member_name in dir(obj):

        method = getattr(obj, member_name)

        if not callable(method) or not hasattr(method, "__route__"):
            continue

        rule, options = getattr(method, "__route__")

        try:
            bind_obj.add_url_rule(rule, view_func=method, **options)
        except Exception as e:
            logging.error(e)


def main_app(app):

    if issubclass(app, Flask):
        module = sys.modules[app.__module__]
        app = main(**parse_args(sys.argv))(app)
        setattr(module, "app", app)
        setattr(module, "socketio", SocketIO())
    return app


def commit(func):

    from . import db, app
    
    @wraps(func)
    def wrapper(*args, **kwargs):
        with app.app_context():
            ret = func(*args, **kwargs)
            db.session.commit()
        return ret
    
    return wrapper


def admin(func):

    from .user import get_users_by

    @wraps(func)
    def wrapper(*args, **kwargs):
        cookie_token = request.cookies.get("token")
        token = request.headers.get("token", cookie_token)
        user = get_users_by(token=token).first()
        if user and user.admin:
            return func(*args, **kwargs)
        return abort(status.UNAUTHORIZED)
    
    return wrapper

def convert_config(conf):
    return dict((k.lower(), token(v)) for k,v in conf.items())


def module_path(path):
    return join_module_path(path)


def get_int(value, default=0):
    if value.isdigit():
        return int(value)
    return default


def get_bool(value, default=False):
    if value.lower() in ("1", "true"):
        return True
    elif value.lower() in ("0", "false"):
        return False
    return default


def get_list(value, default=[]):
    if value:
        if is_windows():
            return value.split(";")
        return value.split(":")
    return default


@contextmanager
def db_session():
    try:
        yield
    finally:
        from . import db
        db.session.commit()