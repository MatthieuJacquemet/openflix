#!/usr/bin/env python3

from .server import OpenFlix
app = OpenFlix()

from flask_babel import Babel
babel = Babel(app)

from flask_socketio import SocketIO
socket = SocketIO(app)

from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy(app)

from flask_cors import CORS
cors = CORS(app)

from flask_mail import Mail
mail = Mail(app)

from flask_pywebpush import WebPush
push = WebPush(app)

from .auth import HTTPAuth
auth = HTTPAuth(app)

from .access_mngr import AccessManager
access = AccessManager(app)

from .content_api import ContentAPI
content = ContentAPI(app)

from .watcher import WatchNotifier
watcher = WatchNotifier(app)