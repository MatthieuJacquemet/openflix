from flask_httpauth import MultiAuth, HTTPBasicAuth, HTTPTokenAuth
from functools import wraps


class HTTPAuth(MultiAuth):

    def __init__(self, app=None):

        if app is not None:
            self.init_app(app)

    def init_app(self, app):

        self.config = app.config.get_namespace("ACCESS_")
        realm = self.config.get("realm")

        self.basic = HTTPBasicAuth(realm=realm)
        self.token = HTTPTokenAuth(realm=realm)

        super().__init__(self.basic, self.token)

        self.config["realm"] = realm or self.basic.realm


    def verify_password(self, func):
        return self.basic.verify_password(func)


    def verify_token(self, func):
        return self.token.verify_token(func)


    def get_user_roles(self, func):
        self.basic.get_user_roles(func)
        return self.token.get_user_roles(func)


    def set_realm(self, realm):
        self.basic.realm = self.token.realm = realm


    # def login_required(self, func=None, role=None):

    #     def factory(f):
    #         f = MultiAuth.login_required(self, role=role)(f)
    #         @wraps(f)
    #         def wrapper(*args, **kwargs):
    #             ret = f(*args, **kwargs)
    #             self.basic.realm = self.token.realm = self.config["realm"]
    #             return ret

    #         return wrapper

    #     if func is not None:
    #         return factory(func)
    #     return factory

