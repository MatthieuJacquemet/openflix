from openflix.openflix_utils import module_path, get_int, get_bool, get_list
import os

SERVER_NAME                     = os.environ.get("SERVER_NAME")

API_METHODS                     = ("GET", "POST", "PUT", "DELETE")
LOG_FILE                        = os.environ.get("OPENFLIX_LOG_FILE")
LOG_LEVEL                       = os.environ.get("OPENFLIX_LOG_LEVEL", "NOTSET")
KEYDIR                          = os.environ.get("OPENFLIX_KEYDIR", module_path("keys"))
HOST                            = os.environ.get("OPENFLIX_HOST")
PORT                            = get_int(os.environ.get("OPENFLIX_PORT", "8000"))
LANGUAGES                       = ("en",)

WATCHER_WATCH_DIR               = module_path(".")
WATCHER_TMP_FILE                = module_path(".tmp")
WATCHER_EVENT_MASK              = "IN_ALL_EVENTS"
WATCHER_RELOAD_EXTS             = (".html", ".js", ".png", ".jpeg")

SQLALCHEMY_DATABASE_URI         = "sqlite:///:memory:"
SQLALCHEMY_TRACK_MODIFICATIONS  = False

CONTENT_PROVIDERS_DIR           = os.environ.get("CONTENT_PROVIDERS_DIR", module_path("providers"))
CONTENT_PREF_PROVIDER           = os.environ.get("CONTENT_PREF_PROVIDER")
CONTENT_EXTRA_PROVIDERS         = get_list(os.environ.get("CONTENT_EXTRA_PROVIDERS", ""))
CONTENT_LANGUAGES               = ("en-US",)
CONTENT_USE_CACHE               = True
CONTENT_FETCH_TIMEOUT           = 30
CONTENT_AUTHENTIFICATION        = False

MAIL_SERVER                     = os.environ.get("MAIL_SERVER")
MAIL_PORT                       = os.environ.get("MAIL_PORT")
MAIL_USERNAME                   = os.environ.get("MAIL_USERNAME")
MAIL_PASSWORD                   = os.environ.get("MAIL_PASSWORD")

WEBPUSH_VAPID_PRIVATE_KEY       = os.environ.get("WEBPUSH_VAPID_PRIVATE_KEY", "")
WEBPUSH_SENDER_INFO             = os.environ.get("WEBPUSH_SENDER_INFO", "")

ACCESS_REQUIRE_ACTIVATION       = get_bool(os.environ.get("ACCESS_REQUIRE_ACTIVATION", ""))
ACCESS_REALM                    = os.environ.get("ACCESS_REALM")

SOCKETIO_HOST                   = "127.0.0.1"
SOCKETIO_ASYNC_MODE             = "threading"