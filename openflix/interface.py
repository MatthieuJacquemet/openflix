from flask import Blueprint, request

from inspect import isclass
from functools import update_wrapper
from http import HTTPStatus as status
from functools import wraps
from . import app, auth

from pyutils.common import token, get_err


class Interface:

    def __init__(self, name, **kwargs):

        auth_roles = kwargs.pop("auth", {})
        kwargs.setdefault("url_prefix", "/")
        self._blueprint = Blueprint(name, self.__class__.__name__)

        if hasattr(self, "__expose__"):

            _, options = getattr(self, "__expose__")

            options.setdefault("methods", app.config["API_METHODS"])

            route_dec = self._blueprint.route("/<name>", **options)
            wrapper = self._interface
            route_dec(update_wrapper(wrapper, self._interface))

        for member_name in dir(self):
            method = getattr(self, member_name)

            if not callable(method):
                continue

            if hasattr(method, "__expose__"):
                rule, options = getattr(method, "__expose__")
                options["methods"] = ("POST",)
                method = self.gen_wrapper(method)

            elif hasattr(method, "__route__"):
                rule, options = getattr(method, "__route__")
            else:
                continue

            namespace = options.pop("auth", None)

            if namespace and namespace in auth_roles:
                role = auth_roles[namespace]
                method = auth.login_required(role)(method)

            self._blueprint.add_url_rule(rule, view_func=method, **options)

        with app.app_context():
            app.register_blueprint(self._blueprint, **kwargs)


    def gen_wrapper(self, method):

        @wraps(method)
        def _wrapper():

            try:
                if request.data:
                    args = request.json
                else:
                    args = dict()
                return str(method(**args)) , status.OK
            except TypeError as e:
                return get_err(e), status.BAD_REQUEST

        return _wrapper


    def _interface(self, name):

        try:
            member = getattr(self, name)

            if ((callable(member) and hasattr(member, "__expose__"))
                    or name == "_interface"):
                raise AttributeError()

            if request.method == "POST":
                if request.data:
                    args = request.json
                else:
                    args = dict()
                return str(member(**args)) + "\n", status.OK

            elif request.method == "GET":
                return str(member) + "\n", status.OK

            elif request.method == "PUT":
                setattr(self, name, token(request.data))

            elif request.method == "DELETE":
                delattr(self, name)

        except TypeError as e:
            return get_err(e), status.BAD_REQUEST
        except AttributeError as e:
            return get_err(e), status.BAD_REQUEST

        return "", status.OK


def expose(rule, **options):

    def factory(func):
        setattr(func, "__expose__", (rule, options))
        return func

    if callable(rule) or isclass(rule):
        func = rule
        rule = "/" + rule.__name__.lower()
        return factory(func)

    return factory