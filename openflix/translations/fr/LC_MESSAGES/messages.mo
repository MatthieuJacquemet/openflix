��          �               �  &   �  �   �     H     g     �     �     �  
   �     �     �     �     �  	             '     *  +   3     _     r     z     �  &   �  +   �  �  �  '   �  �   �      R  "   s     �     �     �     �     �     �     �  !        /     3     <     ?  3   L  &   �  
   �     �     �  -   �  /      %(f)s %(l)s (%(e)s) has just signed up Your account must be verified by admins before being activated. Allow notifications to get notified when your request gets treated. access denied for %(userinfo)s access granted for %(userinfo)s confirm password email address email already in use first name first name omitted invalid email invalid encoding invalid user token last name last name omitted ok password password must be at least 8 characters long passwords mismatch sign up subscribtion success successfuly signed up your openflix account is now activated your openflix registration has been refused Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2020-07-29 16:29+0200
PO-Revision-Date: 2020-07-25 19:52+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 %(f)s %(l)s (%(e)s) vient de s'inscrire Vôtre compte doit être vérifié par un administrateur avant d'être activé.Autorizez les notifications afin d'être averti dès que vôtre demande est traitée. accès refusé pour %(userinfo)s accès autorisé pour %(userinfo)s confirmez le mot de passe adresse email email déja utilisé prénom prénom omis email non valide encodage non valide jeton d'identification non valide nom nom omis ok mot de passe le mot de passe doit avoir au minimum 8 caractères les mots de passe ne correspondent pas s'inscrire enregistrement réussi inscription réussie vôtre compte openflix est maintenant activé vôtre inscription à openflix a été refusée 