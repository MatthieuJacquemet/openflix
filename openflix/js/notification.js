var public_vapid
var registration;

function log(msg) {
    let body = $("body")
    body.html(body.html() + msg + "<br/>")
}

function get_cookie(cname) {

    var name = cname + "="
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(";")

    for (var i = 0; i < ca.length; i++) {

        var c = ca[i]

        while (c.charAt(0) == " ")
            c = c.substring(1)

        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length)
    }
    return ""
}

function subscribe_user() {

    if (!is_subscribed) {
        registration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: public_vapid
        })
        .then(function(subscription) {
            send_subscription(subscription)
            is_subscribed = true
        })
        .catch(function(err) {
            console.log("failed to subscribe the user: ", err)
        })
    } else {
        registration.pushManager.getSubscription()
        .then(function (subscription) {
            if (subscription) {
                send_subscription(subscription)
            }
        })
        .catch(function (error) {
            console.log("error getting subscribtion : ", error);
        })
    }
}

function unsubscribe_user() {

    registration.pushManager.getSubscription()
    .then(function (subscription) {
        if (subscription)
            return subscription.unsubscribe()
    })
    .catch(function (error) {
        console.log("error unsubscribing", error);
    })
    .then(function () {
        send_subscription(null);

        console.log("user is unsubscribed");
        is_subscribed = false;
    })
}

function init_notification(callback) {

    if ("serviceWorker" in navigator && "PushManager" in window) {
        console.log("Service Worker and Push is supported")

        navigator.serviceWorker.register("/js/sw.js").then(function(sw) {
            console.log("Service Worker is registered")
            registration = sw

            registration.pushManager.getSubscription()
            .then(function(subscription) {
                is_subscribed = !(subscription === null)

                if (is_subscribed)
                    console.log("user is subscribed")
                else 
                    console.log("user is not subscribed")

                callback()
            })

        }).catch(function(error) {
            console.error("Service Worker error", error)
        })
    } else
        console.warn("Push messaging is not supported")
}

function send_subscription(data) {

    let token = get_cookie("token")

    if (token) {
        $.post("/subscribe", {subscribtion: JSON.stringify(data),
                              user_token: token})
    } else
        console.log("cannot subscribe unidentified user")
}

function fetch_vapid(callback) {

    $.get("/vapid/der").done(function (data) {

        let raw = window.atob(data)
        let array = new Uint8Array(raw.length);

        for (let i = 0; i < data.length; i++) {
            let c = raw.charCodeAt(i);
            array[i] = c & 0xff
        }

        public_vapid = array.slice(-65);

        callback()
    })
}


// function update_button() {
//     if (Notification.permission === "denied") {
//         button.textContent = "Push Messaging Blocked."
//         button.disabled = true;
//         send_subscription(null);
//         return;
//     }
    
//     if (is_subscribed) {
//         button.textContent = "Disable Push Messaging"
//     } else {
//         button.textContent = "Enable Push Messaging"
//     }
    
//     button.disabled = false
// }
