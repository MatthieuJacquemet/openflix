self.addEventListener('push', function (event) {

    let raw = event.data.text()
    let data, title, body, options

    try {
        data = JSON.parse(raw)
        title = data.title
        body = data.body
    } catch (e) {
        title = ""
        body = ""
    }

    if (data.hasOwnProperty("grant_link") && 
        data.hasOwnProperty("deny_link") && 
        data.hasOwnProperty("bearer")) 
    {    
        options = {
            body: body,
            data: {
                grant_link: data.grant_link,
                deny_link: data.deny_link,
                bearer: data.bearer
            },
            actions: [
                {action: "grant", title: "grant user", 
                    icon: "/static/images/check.svg"},
                {action: "deny", title: "deny user",
                    icon: "/static/images/close.svg"},
            ]
        }
    } else {
        options = {body: body}
    }

    event.waitUntil(self.registration.showNotification(title, options))
})


self.addEventListener('notificationclick', function (event) {

    let link
    let notification = event.notification
    notification.close()

    if (event.action == "grant")
        link = notification.data.grant_link
    else if (event.action == "deny")
        link = notification.data.deny_link
    else
	return

    let bearer = "Bearer " + notification.data.bearer

    return fetch(link, {method: "POST", 
                        headers: {"Authorization": bearer}})
})

