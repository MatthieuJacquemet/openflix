
from flask import (abort, request, make_response, render_template, Response, 
    jsonify, url_for, send_file)

from http import HTTPStatus as status
from flask_mail import Message
from queue import Queue, Empty
from threading import Thread, Event, RLock
from functools import wraps
from . import mail, db, auth, push, app, babel
from .interface import Interface
from .openflix_utils import route, sqla_to_object
from .user import get_users_by, add_user
from werkzeug.security import check_password_hash
from py_vapid.utils import b64urldecode
from cryptography.hazmat.primitives import serialization
from py_vapid import Vapid
from pywebpush import WebPushException
from flask_babel import _

from datetime import datetime
import secrets
import base64
import logging
import time
import string
import json
import re
import os
import time

logger = logging.getLogger(__name__)


class PushSubscription(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.Text, nullable=False)
    user_token = db.Column(db.String(64), nullable=False)

    def __repr__(self):
        return f"data : {self.data}"


def add_subscription(token, data):

    with app.app_context():
        sub = PushSubscription(data=data, user_token=token)
        db.session.add(sub)
        db.session.commit()


def del_subscription(**kwargs):

    with app.app_context():
        subs = PushSubscription.query.filter_by(**kwargs).all()
        for subscription in subs:
            db.session.delete(subscription)


def get_subscription(**kwargs):

    with app.app_context():
        return PushSubscription.query.filter_by(**kwargs)


@auth.verify_password
def verify_password(email, password):

    user =  get_users_by(email=email).first()
    if user and user.active:
        if check_password_hash(user.password_hash, password):
            return user


@auth.verify_token
def verify_token(token):

    user = get_users_by(token=token).first()
    if user and user.active:
        return user



@auth.get_user_roles
def get_user_roles(user):
    return user.role


@babel.localeselector
def get_locale():
    lang = app.config['LANGUAGES']
    return request.accept_languages.best_match(lang)


def to_htime(seconds):

    if seconds > 86400:
        return f"{int(seconds/86400)} days"
    if seconds > 3600:
        return f"{int(seconds/3600)} hours"
    if seconds > 60:
        return f"{int(seconds/60)} minutes"

    return f"{seconds} seconds"


def is_valid_email(email):

    if email and re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return True
    return False


def is_valid_password(password):

    return len(password) >= 8


class AccessManager(Interface):

    def __init__(self, app=None):
        super().__init__("access")

        self.app = app
        self._vapid_cache = dict()

        if self.app is not None:
            self.init_app(app)


    def init_app(self, app):
        self.app = app
        self.load_config()


    def load_config(self):

        self.config = self.app.config.get_namespace("ACCESS_")


    @route("/admin")
    @auth.login_required(role="admin")
    def admin(self):

        user = auth.current_user()
        data = render_template("admin.html", debug=self.app.debug)

        resp = make_response(data, status.OK)
        resp.set_cookie("token", user.token)
        return resp


    @route("/signup")
    def signup(self):
        return render_template("signup.html", debug=self.app.debug, 
                    require_activation=self.config["require_activation"])
            

    @route("/signup", methods=("POST",))
    def submit_signup(self):

        firstname = request.form.get("firstname")
        lastname = request.form.get("lastname")
        password = request.form.get("password")
        email = request.form.get("email")

        if not is_valid_email(email):
            error = _("invalid email")
        elif not is_valid_password(password):
            error = _("password must be at least 8 characters long")
        elif not firstname:
            error = _("first name omitted")
        elif not lastname:
            error = _("last name omitted")
        elif get_users_by(email=email).first():
            error = _("email already in use")
        else:
            active = not self.config["require_activation"]
            user = add_user(firstname, lastname, email, password, active)

            if not active:
                self.notify_signup(user)

            resp = make_response(_("successfuly signed up"))
            resp.set_cookie("token", user.token)
            return resp

        return error, status.BAD_REQUEST


    @route("/account/<token>/<action>", methods=("POST",))
    @auth.login_required(role="admin")
    def activate_account(self, token, action):

        user = get_users_by(token=token).first()

        if user is None:
            return _("invalid user token"), status.UNAUTHORIZED

        userinfo = f"{user.firstname} {user.lastname} ({user.email})"

        if action == "grant":
            user.active = True
            msg = _("access granted for %(userinfo)s", userinfo=userinfo)
        elif action == "deny":
            user.active = False
            msg = _("access denied for %(userinfo)s", userinfo=userinfo)
        else:
            return "", status.BAD_REQUEST

        user = sqla_to_object(user)
        db.session.commit()

        self.notify_activation(user)

        return msg, status.OK


    @route("/vapid/<encoding>")
    def get_vapid(self, encoding):

        private_key = self.app.config["WEBPUSH_VAPID_PRIVATE_KEY"]

        if encoding == "pem":
            enc = serialization.Encoding.PEM
        elif encoding == "der":
            enc = serialization.Encoding.DER
        elif encoding == "openssh":
            enc = serialization.Encoding.OpenSSH
        elif encoding == "x962":
            enc = serialization.Encoding.X962
        else:
            return _("invalid encoding"), status.BAD_REQUEST

        if encoding in self._vapid_cache:
            data = self._vapid_cache[encoding]
        else:
            if os.path.isfile(private_key):
                vapid = Vapid.from_file(private_key)
            else:
                vapid = Vapid.from_string(private_key)

            if enc == serialization.Encoding.OpenSSH:
                fmt = serialization.PublicFormat.OpenSSH
            else:
                fmt = serialization.PublicFormat.SubjectPublicKeyInfo

            raw = vapid.public_key.public_bytes(enc, fmt)
            data = base64.b64encode(raw)

            self._vapid_cache[encoding] = data

        return data, status.OK


    @route("/subscribe", methods=("POST",))
    def subscribe(self):

        data = request.form.get("subscribtion")
        user = request.form.get("user_token")

        if get_users_by(token=user).first() is not None:
            if data:
                if get_subscription(user_token=user,data=data).first() is None:
                    add_subscription(user, data)
            else:
                del_subscription(user_token=user)
        else:
            return _("invalid user token"), status.BAD_REQUEST

        return _("subscribtion success"), status.CREATED


    def notify_signup(self, user):

        admins = get_users_by(role="admin").all()

        for admin in admins:
            for sub in get_subscription(user_token=admin.token).all():

                grant_link  = url_for( "access.activate_account", 
                                token=user.token,
                                action="grant")

                deny_link   = url_for( "access.activate_account", 
                                token=user.token,
                                action="deny")

                msg = _("%(f)s %(l)s (%(e)s) has just signed up", 
                            f=user.firstname, l=user.lastname, e=user.email)

                notification = {"title": "", 
                                "body": msg,
                                "grant_link": grant_link,
                                "deny_link": deny_link,
                                "bearer": admin.token}

                data = json.loads(sub.data)

                try:
                    push.send(data, notification)
                except WebPushException as e:
                    if e.response.status_code == status.GONE:
                        del_subscription(id=sub.id)
                except Exception as e:
                    logger.error(e)


    def notify_activation(self, user):

        subscription = get_subscription(user_token=user.token).first()

        if subscription is not None:

            if user.active:
                msg = _("your openflix account is now activated")
            else:
                msg = _("your openflix registration has been refused")

            notification = {"title": "", "body": msg}
            data = json.loads(subscription.data)

            try:
                push.send(data, notification)
            except WebPushException as e:
                if e.response.status_code == status.GONE:
                    del_subscription(id=subscription.id)
            except Exception as e:
                logger.error(e)
