from .user import User, add_user
import openflix.views
from .openflix_utils import db_session, get_bool
from . import app, db, socket, watcher, app


if __name__ == "__main__":

    db.create_all()

    if app.debug:
        watcher.start()

    if app.config["TESTING"] is True:
        add_user("admin", "admin", "admin", "admin", True, "admin")

    app.run(use_reloader=False)
    socket.run(app)
