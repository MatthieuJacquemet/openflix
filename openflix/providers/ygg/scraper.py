import re
import os
import time
import datetime
import copy

from threading import Timer
from .categories import categories
from bs4 import BeautifulSoup
from unidecode import unidecode
from http import HTTPStatus as status
from . import patterns
from . import cfbypass
import requests
from requests import sessions
# import cloudscraper
# import cfscrape

from .parse import PTN
# from categories import categories
# import patterns

YGG_MASTO_URL = "https://mamot.fr/@YggTorrent"
YGG_DOMAIN_PATTERN = r"[^\.]+\.yggtorrent\.([^/]+/"

YGG_DOMAIN = f".yggtorrent.si"
YGG_FULL_DOMAIN = f"www3{YGG_DOMAIN}"

YGG_BASE_URL = f"https://{YGG_FULL_DOMAIN}"
YGG_LOGIN_URL = f"{YGG_BASE_URL}/user/login"
YGG_LOGOUT_URL = f"{YGG_BASE_URL}/user/logout?attempt=1"
YGG_SEARCH_URL = f"{YGG_BASE_URL}/engine/search?"
YGG_SETTING_URL = f"{YGG_BASE_URL}/user/settings"

YGG_TOKEN_COOKIE = "ygg_"

YGG_GET_FILES = f"{YGG_BASE_URL}/engine/get_files?torrent="
YGG_GET_INFO = f"{YGG_BASE_URL}/engine/get_nfo?torrent="
YGG_GET_TORRENT = f"{YGG_BASE_URL}/engine/download_torrent?id="

YGG_MOST_COMPLETED_URL = f"{YGG_BASE_URL}/engine/mostcompleted"

TORRENT_PER_PAGE = 50


def get_domain(url):

    match = re.search(r"([^/\.]+\.yggtorrent\.[^/]+)/?", url)
    if match:
        return match.group(1)
    return ""


def set_ygg_base_url(url_base):

    global YGG_DOMAIN
    global YGG_FULL_DOMAIN
    global YGG_BASE_URL
    global YGG_LOGIN_URL
    global YGG_LOGOUT_URL
    global YGG_SEARCH_URL
    global YGG_GET_FILES
    global YGG_GET_INFO
    global YGG_GET_TORRENT
    global YGG_MOST_COMPLETED_URL
    global YGG_SETTING_URL

    YGG_DOMAIN = ".".join(get_domain(url_base).split('.')[1:])
    YGG_FULL_DOMAIN = f"www3.{YGG_DOMAIN}"
    YGG_BASE_URL = f"https://{YGG_FULL_DOMAIN}"

    YGG_LOGIN_URL = f"{YGG_BASE_URL}/user/login"
    YGG_LOGOUT_URL = f"{YGG_BASE_URL}/user/logout?attempt=1"

    YGG_SEARCH_URL = f"{YGG_BASE_URL}/engine/search?"
    YGG_GET_FILES = f"{YGG_BASE_URL}/engine/get_files?torrent="
    YGG_GET_INFO = f"{YGG_BASE_URL}/engine/get_nfo?torrent="
    YGG_GET_TORRENT = f"{YGG_BASE_URL}/engine/download_torrent?id="
    YGG_SETTING_URL = f"{YGG_BASE_URL}/user/settings"

    YGG_MOST_COMPLETED_URL = f"{YGG_BASE_URL}/engine/mostcompleted"


def find_first(items, name):

    for item in items:
        if item.get("name") == name:
            return item


def build_query_string(args):

    def build_args():
        for key, value in args:
            yield f"{key}={value.replace(' ','+')}"

    r = "&".join(build_args())
    return r


def get_options(section, data):

    ret = list()

    for key, values in data.get("options",()).items():

        option = find_first(section, key)
        if option is None:
            continue

        for value in values:
            try:
                index = option["values"].index(value)
            except ValueError:
                continue
                
            name = "option_" + option['name']
            if "multiple" in option:
                name += "%3Amultiple"

            ret.append((name + "%5B%5D", str(index+1)))
        
    return ret


def ceate_category_qs(data):

    args = list()

    if "name" in data:
        args.append(("name", data["name"]))

    if "page" in data:
        args.append(("page", data["page"]))

    if "descriptions" in data:
        args.append(("descriptions", "+".join(data["descriptions"])))

    if "files" in data:
        args.append(("files", "+".join(data["files"])))

    if "uploader" in data:
        args.append(("uploader", data["uploader"]))

    if "sort" in data:
        args.append(("sort", data["sort"]))

    if "order" in data:
        args.append(("order", data["order"]))


    if "category" in data:

        category = find_first(categories, data["category"])
        
        args.append(("category", category["id"]))

        sub = category["subcategories"]

        if "subcategory" in data:

            subcategory = find_first(sub, data["subcategory"])
            args.append(("sub_category", subcategory["id"]))

            section = subcategory["options"]
            args.extend(get_options(section, data))

        elif data["category"] == "films_&_videos":

            options = data.get("options", {})

            if options.keys() <= {"langue", "qualite"}:
                section = sub[0]["options"]
                args.extend(get_options(section, data))

    args.append(("do","search"))

    return args


def create_search_url(options):

    args = ceate_category_qs(options)
    return YGG_SEARCH_URL + build_query_string(args)


def is_full_season(name):

    name = unidecode(name).lower()

    return (re.match(r"[ _\-\.]+s(eason|aison)?[ _\-\.]+[0-9]+[ _\-\.]+",name)
        and not re.match(r"[ _\-\.]+e(pisode)?[ _\-\.]+[0-9]+[ _\-\.]+",name))


def parse_torrent_name(name):

    # patch patterns for ygg
    # PTN.patterns.patterns = patterns.patterns
    # PTN.patterns.types = patterns.types
    ptn = PTN()
    return ptn.parse(name)


def to_bytes(size):

    value, unit = float(size[:-2]), size[-2]

    if unit == "G":
        value *= 10**9
    elif unit == "M":
        value *= 10**6
    elif unit == "k":
        value *= 10**3

    return int(value)


def get_episode_from_options(data):

    options = data.get("options")

    if options:
        episode = options.get("episode")
        if episode and episode[0].startswith("episode"):
            return int(episode[0][-2:])


def get_torrent_id(link):

    match = re.match("/([0-9]+)-", link)
    if match:
        return int(match.group(1))


def is_video(filename):

    video_ext = (".mkv", ".mp4", ".avi", ".flv", ".mov")
    _, ext = os.path.splitext(filename)
    return ext in video_ext


def t_to_sec(value):

    if not isinstance(value, str):
        return value

    t = time.strptime(value, "%H:%M:%S")

    return datetime.timedelta(hours=t.tm_hour,
                              minutes=t.tm_min,
                              seconds=t.tm_sec).total_seconds()


class Torrent:

    def __init__(self):
        
        self.type       = 0
        self.name       = ""
        self.url        = ""
        self.id         = -1
        self.age        = -1
        self.size       = -1
        self.completed  = -1
        self.seeders    = -1
        self.leechers   = -1
        self.info       = dict()


    def load_data(self, tag):
        
        tds = tag.find_all("td", recursive=False)

        self.type       = tds[0].find("div").text.strip()
        self.name       = tds[1].find("a").text.strip()
        self.url        = tds[1].find("a")["href"]
        self.id         = tds[2].find("a")["target"]
        self.age        = tds[4].find("span").text.strip()
        self.size       = to_bytes(tds[5].text)
        self.completed  = int(tds[6].text)
        self.seeders    = int(tds[7].text)
        self.leechers   = int(tds[8].text)
        self.info       = parse_torrent_name(self.name)


    def iter_files(self, session=None):

        session = session or requests
        response = session.get(YGG_GET_FILES + str(self.id))
        data = response.content.decode("unicode-escape", "ignore")
        files_page = BeautifulSoup(data, features="lxml")

        file_tags = files_page.find_all("tr")

        for file_tag in file_tags:

            torrent_file = TorrentFile(self)
            td_tags = file_tag.find_all("td")
            
            torrent_file.size = to_bytes(td_tags[0].text.strip())
            torrent_file.filename = td_tags[1].text.strip()
                
            yield torrent_file


    def find_episode_file(self, episode_num, session=None):

        if not "episode" in self.info:

            for file in self.iter_files(session):

                if is_video(file.filename):
                    _, episode = file.get_show_info()

                    if episode == episode_num:
                        return file


class TorrentFile:

    def __init__(self, torrent):

        self.torrent = torrent
        self.filename = ""
        self.size = -1
        self.type = torrent.type
        self.seeders = torrent.seeders
        self.id = torrent.id
        self.info = torrent.info

    def get_show_info(self):

        season = None
        episode = None

        for pattern_name, pattern in patterns.patterns:

            if pattern_name in ("season", "episode"):

                match = re.search(pattern, self.filename, re.I)
                if not match:
                    continue

                data = int(match.group(2))
                if pattern_name == "season":
                    season = data
                elif pattern_name == "episode":
                    episode = data
        
        return season, episode


class YGG_Scraper:

    def __init__(self, app=None, session=None):

        # self.session = session or requests.Session()
        # self.session = session or cloudscraper.create_scraper()
        self.session = session or cfbypass.CFBypass()
        # c = self.session.get_cookie_string("https://www3.yggtorrent.re/")

        self.app = app
        self.logged = False

        if self.app is not None:
            self.init_app(app)


    def init_app(self, app):

        self.app = app
        self.load_config()
        self.update_domain()


    def load_config(self):

        self.config = self.app.config.get_namespace("YGGSCRAPER_")

        self.config["domain_interval"]=t_to_sec(self.config["domain_interval"])
        self.config["login_interval"]=t_to_sec(self.config["login_interval"])


    def dual_request(self, url, **kwargs):
        
        kwargs.setdefault("method", "GET")
        kwargs.setdefault("timeout", self.config["timeout"])
        kwargs.setdefault("url", url)
    
        try:
            response = self.session.request(**kwargs)
        except requests.exceptions.Timeout:
            with sessions.Session() as session:
                return session.request(**kwargs)

        return response


    def store_cookie(self, *args, **kwargs):

        response = self.session.request(*args, **kwargs)
    
        if response.status_code == status.OK:
            
            cookies = response.cookies.get_dict()
            login_token = cookies.get(YGG_TOKEN_COOKIE)

            if login_token is not None:

                cookie = requests.cookies.create_cookie(
                    domain=YGG_DOMAIN,
                    name=YGG_TOKEN_COOKIE,
                    value=login_token,
                )
                self.session.cookies.set_cookie(cookie)

        return response



    def login(self, username, password):

        # headers = {
        #     "User-Agent": "PostmanRuntime/7.17.1",
        #     "Content-Type": "application/x-www-form-urlencoded",
        #     "Accept": "*/*",
        #     "Cache-Control": "no-cache",
        #     "Host": f"www.{YGG_DOMAIN}",
        #     "Accept-Encoding": "gzip, deflate",
        #     "Connection": "keep-alive",
        # }

        self.store_cookie("GET", YGG_BASE_URL) # need to load the main page to retrieve ygg_ cookie

        response = self.store_cookie("POST", YGG_LOGIN_URL,
            data = {"id": username, "pass": password},
        )

        if response.status_code == status.OK:
            self.logged = True

            interval = self.config["login_interval"]
            if interval > 0:
                Timer(interval, self.login, (username, password)).start()

            return True

        return False


    def logout(self):

        response = self.session.get(YGG_LOGOUT_URL)
        # self.session.cookies.clear()
        if response.status_code == status.OK:
            self.logged = False
            return True
        return False


    def update_domain(self):

        try:
            resp = requests.get(YGG_MASTO_URL)
        except requests.exceptions.Timeout:
            pass
        else:
            pattern = r'<a href="(https?://[^\.]+\.yggtorrent\.[^/]+)/"'
            match = re.search(pattern, resp.content.decode("utf-8"))
            if match:
                set_ygg_base_url(match.group(1))

        interval = self.config["domain_interval"]
        if interval > 0:
            Timer(interval, self.update_domain).start()


    def find_pages(self, options):

        response = self.session.get(create_search_url(options))
        first_page = BeautifulSoup(response.content, features="lxml")
        pagination = first_page.find("ul", {"class": "pagination"})

        if pagination is not None:
            others = [tag["href"] for tag in pagination.find_all("a")[1:]]
        else:
            others = list()
        
        return first_page, others


    def download_torrent(self, id):

        if self.logged:

            if isinstance(id, Torrent):
                id = id.id
            
            response = self.session.get(YGG_GET_TORRENT + str(id))
            return response.content


    def get_announce_url(self):

        # cookie = requests.cookies.create_cookie(
        #     domain=YGG_DOMAIN,
        #     name=YGG_TOKEN_COOKIE,
        #     value=self.token,
        # )

        # self.session.cookies.set_cookie(cookie)
        # s = requests.Session()
        # s.cookies.set_cookie(cookie)
        # response = s.get(YGG_SETTING_URL)

        response = self.session.get(YGG_SETTING_URL)
        page = BeautifulSoup(response.content, features="lxml")

        url = page.select_one("#update-settings > table > tr > td + td")
        if url is not None:
            return url.text

        return None


    def list_torrents(self, page):

        for tag in page.find_all("a", {"id": "torrent_name"}):
            
            torrent = Torrent()
            torrent.load_data(tag.parent.parent)

            yield torrent


    def iter_all_torrents(self, options):

        first_page, others = self.find_pages(options)
        yield from self.list_torrents(first_page)

        for page_url in others:
            response = self.session.get(page_url)
            page = BeautifulSoup(response.content, features="lxml")

            yield from self.list_torrents(page)
