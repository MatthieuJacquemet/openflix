import io
import logging
import os
import sys
import zipfile
from pyutils.common import is_linux, is_windows, is_mac
from urllib.request import urlopen, urlretrieve

from selenium.webdriver import Chrome as _Chrome
from selenium.webdriver import ChromeOptions as _ChromeOptions

logger = logging.getLogger(__name__)


TARGET_VERSION = 81
DL_BASE = "https://chromedriver.storage.googleapis.com"


class Chrome(_Chrome):

    def __init__(self, *args, **kwargs):
        
        kwargs.setdefault("options", ChromeOptions())

        kwargs["executable_path"] = self.install(**kwargs)
        kwargs.pop("target_version", None)
        
        super().__init__(*args, **kwargs)

        self.patch_js()

    def install(self, target_version=None, executable_path=None, **_):

        self.target_version = target_version or TARGET_VERSION
        self._base = base_ = "chromedriver{}"

        exe_name = self._base
        platform = sys.platform

        if is_windows():
            exe_name = base_.format(".exe")
        else:
            exe_name = exe_name.format("")

            if is_linux():
                platform += "64"
            elif is_mac():
                platform = "mac64"

        self.platform = platform
        self.executable_path = executable_path or f"./{exe_name}"
        self.exe_name = exe_name

        if not os.path.isfile(self.executable_path):
            self.fetch_chromedriver()

        self.patch_binary()

        return self.executable_path

    def patch_js(self):

        self.execute_cdp_cmd(
            "Page.addScriptToEvaluateOnNewDocument",
            {
                "source": """
        Object.defineProperty(window, "navigator", {
            value: new Proxy(navigator, {
              has: (target, key) => (key === "webdriver" ? false : key in target),
              get: (target, key) =>
                key === "webdriver"
                  ? undefined
                  : typeof target[key] === "function"
                  ? target[key].bind(target)
                  : target[key]
            })
        })
                  """
            },
        )
        original_user_agent_string = self.execute_script(
            "return navigator.userAgent"
        )
        self.execute_cdp_cmd(
            "Network.setUserAgentOverride",
            {
                "userAgent": original_user_agent_string.replace("Headless", ""),
                "platform": "Windows",
            },
        )

    def get_release_version_number(self):

        path = (
            "/LATEST_RELEASE"
            if not self.target_version
            else f"/LATEST_RELEASE_{self.target_version}"
        )
        return urlopen(DL_BASE + path).read().decode()

    def patch_binary(self):
        
        patch_mark = b"__patched__"

        with io.open(self.executable_path, "r+b") as binary:

            binary.seek(-len(patch_mark),os.SEEK_END)
            data = binary.read()

            if data == patch_mark:
                return True

            binary.seek(0)

            for line in iter(lambda: binary.readline(), b""):

                if b"cdc_" in line:
                    binary.seek(-len(line), os.SEEK_CUR)
                    line = b"  var key = '$azc_abcdefghijklmnopQRstuv_';\n"
                    binary.write(line)
                    break
            else:
                return False

            binary.seek(0,os.SEEK_END)
            binary.write(patch_mark)

        return True

    def fetch_chromedriver(self):

        base_ = self._base
        zip_name = base_.format(".zip")
        ver = self.get_release_version_number()

        filename = base_.format(f"_{self.platform}")

        urlretrieve(
            f"{DL_BASE}/{ver}/{filename}.zip",
            filename=zip_name,
        )

        with zipfile.ZipFile(zip_name) as zf:
            zf.extract(self.exe_name)
        
        os.remove(zip_name)

        if sys.platform != "win32":
            os.chmod(self.exe_name, 0o755)

        self.executable_path = f"./{self.exe_name}"


class ChromeOptions(_ChromeOptions):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.add_argument("headless")
        self.add_argument("no-sandbox")
        self.add_argument("disable-gpu")

        self.add_experimental_option("excludeSwitches", ["enable-automation"])
        self.add_experimental_option("useAutomationExtension", False)
