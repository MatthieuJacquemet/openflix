# Copyright (C) 2022 Matthieu Jacquemet
# 
# This file is part of openflix.
# 
# openflix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# openflix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with openflix.  If not, see <http://www.gnu.org/licenses/>.



from playwright.sync_api import sync_playwright
from cf_clearance import sync_retry, stealth_sync
import requests
import logging


logger = logging.getLogger(__name__)



class CFBypass(requests.Session):

    def __init__(self, *args, **kwargs):

        super().__init__()
        self.load_config(kwargs)
        self.__cf_clearance_value = ""
        self.__ua = ""

    @staticmethod
    def is_iuam_challenge(resp):

        return (
            resp.status_code in (503, 429)
            and resp.headers.get("Server", "").startswith("cloudflare")
            and b"jschl_vc" in resp.content
            and b"jschl_answer" in resp.content
        )


    def invoke_request(self, method, url, *args, **kwargs):

        headers = kwargs.setdefault("headers", {})
        cookies = kwargs.setdefault("cookies", {})

        headers.update({"User-Agent": self.__ua})
        cookies.update({"cf_clearance": self.__cf_clearance_value})

        return super().request(method, url, *args, **kwargs)


    def request(self, method, url, *args, **kwargs):

        resp = self.invoke_request(method, url, *args, **kwargs)

        # The cf_clearance token might be outdated, lets try to get a new one
        if self.is_iuam_challenge(resp):
            self.solve_challenge(url)
            resp = self.invoke_request(method, url, *args, **kwargs)
    
        return resp


    def load_config(self, options):
        self.config = options
        self.config.setdefault("cookies", ( "__cf_bm", "__cfduid", "__ga",
                                    "_gaexp", "cf_clearance"))


    def solve_challenge(self, url):

        with sync_playwright() as p:

            browser = p.chromium.launch(headless=False)
            page = browser.new_page()
            stealth_sync(page)
            page.goto(url)

            if sync_retry(page):
                cookies = page.context.cookies()
                self.__ua = page.evaluate('() => {return navigator.userAgent}')
                for cookie in cookies:
                    if cookie.get('name') == "cf_clearance":
                        self.__cf_clearance_value = cookie.get("value")           
            else:
                logger.error("cf challenge fail")

            browser.close()
        
        return self.__cf_clearance_value


                        # cookie_obj = self.cookies.create_cookie(
                        #     domain=YGG_DOMAIN,
                        #     name=YGG_TOKEN_COOKIE,
                        #     value=login_token,
                        # )
                        # self.cookies.set_cookie(cookie_obj)