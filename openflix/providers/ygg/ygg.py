import time
import datetime
import requests
import re
import logging
# import torrent_parser
import string

import bencode

from openflix.interface import Interface, expose
from openflix import app, config
# from mastodon import Mastodon, MastodonError

from http import HTTPStatus as status
from flask import Response, abort, request, url_for
from openflix.openflix_utils import route, admin
from pyutils.thread import thread
from collections import deque

# from yggtorrentscraper import YggTorrentScraper
from random import randint
from openflix import app
from difflib import SequenceMatcher

from .scraper import (YGG_Scraper, to_bytes, TorrentFile, Torrent, 
    get_episode_from_options, find_first)

from .categories import categories

logger = logging.getLogger(__name__)
# import cloudscraper

# def get_category(name):

#     for category in categories:
#         if category["name"] == name:
#             return category


def get_first_occ(fields, name):

    for field in fields:
        if field.name == name:
            return field


def clean_title(title):

    rep_sym = ("&",)
    tr = dict.fromkeys(string.punctuation, " ")
    tr.update(dict.fromkeys(rep_sym, "*"))

    return title.translate(str.maketrans(tr))


def convert_options(options):

    data = {"options":dict()}


    media_table = { "movie":("films_&_videos", ("film", 
                                                "animation")),

                    "show": ("films_&_videos", ("emission_tv",
                                                "serie_tv",
                                                "animation_serie")),

                    "game": ("jeux_video", options.get("platform", ()))}


    lang_table = {  r"^fr(-FR)?$" : {   "francais_(vff/truefrench)", 
                                        "multi_(francais_inclus)",
                                        "vfstfr"},
    
                    r"^fr(-CA)?$" : {   "quebecois_(vfq/french)",
                                        "multi_(quebecois_inclus)"},

                    r"^en(-.+)?$" : {   "anglais", "vostfr"}}


    res_table = {   "4k" : {    "bluray_4k_[full_ou_remux]",
                                "hdrip_4k_[rip_hd_4k_depuis_source_4k]",
                                "tvriphd_4k_[rip_hd_4k_depuis_source_tv_4k]",
                                "web-dl_4k",
                                "webrip_4k"},

                    "fullhd" : {"bluray_[full]",
                                "bluray_[remux]",
                                "hdrip_1080_[rip_hd_depuis_bluray]",
                                "hdrip_1080_[rip_hd_depuis_bluray]",
                                "tvriphd_1080_[rip_hd_depuis_source_tv_hd]",
                                "web-dl_1080",
                                "webrip_1080"},

                    "hd" : {    "hdrip_720_[rip_hd_depuis_bluray]",
                                "tvriphd_720_[rip_hd_depuis_source_tv_hd]",
                                "web-dl_720",
                                "webrip_720"},

                    "sd" : {    "bdrip/brrip_[rip_sd_(non_hd)_depuis_bluray_ou_hdrip]",
                                "dvd-r_5_[dvd_<_4.37gb]",
                                "dvd-r_9_[dvd_>_4.37gb]",
                                "dvdrip_[rip_depuis_dvd-r]",
                                "tvrip_[rip_sd_(non_hd)_depuis_source_tv_hd/sd]",
                                "vcd/svcd/vhsrip",
                                "web-dl",
                                "webrip"}}


    media_type = options.get("type", "movie")

    if media_type in media_table:
        data["category"], data["subcategory"] = media_table[media_type]


    for lang in options.getlist("language"): # or ("fr",):

        for pattern, opts in lang_table.items():
            if re.match(pattern, lang):

                if not "langue" in data["options"]:
                    data["options"]["langue"] = set()
                
                data["options"]["langue"].update(opts)


    for res in options.getlist("resolution"): # or ("fullhd",):

        if res in res_table:

            if not "qualite" in data["options"]:
                data["options"]["qualite"] = set()
                
            data["options"]["qualite"].update(res_table[res])

    return data


def remove_duplicates(torrents):

    def get_id(torrent):
        return torrent.id, torrent

    return list(dict(map(get_id,torrents)).values())


def select_best_torrents(limit, torrents, max_torrent=4):
    
    def get_score(torrent):
        
        size_score = 1 - abs(torrent.size - limit) / limit
        seed_score = (torrent.seeders - 10) * 0.01
        return size_score + seed_score
    
    torrents = remove_duplicates(torrents)
    torrents.sort(key=get_score, reverse=True)
    
    return torrents[:min(len(torrents), max_torrent)]


def convert_to_content(torrent):
    
    with app.app_context():
        uri = url_for("ygg.download_torrent", id=torrent.id, _external=True)

##################### temp fix ###########################

    # uri = torrent.url

##################### temp fix ###########################

    if isinstance(torrent, TorrentFile):
        filename = torrent.filename
        torrent = torrent.torrent
    else:
        filename = None

    return {"provider": "ygg",
            "name": torrent.name,
            "size": torrent.size,
            "type": "torrent",
            "data": {
                "torrent": uri,
                "file": filename,
                "uploaded": torrent.age,
                "completed": torrent.completed,
                "seeders": torrent.seeders,
                "leechers": torrent.leechers
                }
            }


class YGG_Provider(Interface):

    def __init__(self, app=None):
        super().__init__("ygg")

        self.app = app
        self.scraper = None
        self.announce_url = ""
        self.tracker_url = ""

        if self.app is not None:
            self.init_app(app)


    def init_app(self, app):

        self.app = app
        self.load_config()
        self.init_scraper()


    def load_config(self):

        self.config = self.app.config.get_namespace("YGG_")


    @thread
    def init_scraper(self):

        self.scraper = YGG_Scraper(self.app)

        username = self.config["username"]
        password = self.config["password"]

        if not username or not password:
            logger.warn("yggtorrent credentials not found")

        elif self.scraper.login(username, password):
            logger.info("successfuly logged to yggtorrent")
            try:
                self.announce_url = self.scraper.get_announce_url()
                if self.announce_url is None:
                    raise Exception("cannot parse html")
            except Exception as e:
                logger.error(f"failed to get announce url : {e}")
        else:
            logging.warn("could not loggin to yggtorrent")


    def get_max_torrent(self, options):

        n = options.get("max_content", None)

        if n is not None and n.isdigit():
            return int(n)

        return self.config["max_torrent"]


    def iter_torrents(self, options):

        options["sort"] = "size"
        options["order"] = "desc"

        episode_num = get_episode_from_options(options)

        for torrent in self.scraper.iter_all_torrents(options):

            if options.get("type") == "show":
                file = torrent.find_episode_file(episode_num,
                                        self.scraper.session)
                torrent = file or torrent

            yield torrent


    def search_torrents(self, options, max_size, year, alt_name, max_torrents=4):

        torrents = deque(maxlen=max_torrents)
        subcategories = options.get("subcategory")
        subcategories_id = list()
        name = options.get("name").lower()
        alt_name = alt_name.lower()

        if isinstance(subcategories, tuple):
            
            if len(subcategories) > 1:

                options.pop("subcategory", None)
                category = find_first(categories, options.get("category"))

                if category is not None:

                    for subcategory in subcategories:

                        subs = category["subcategories"]
                        sub = find_first(subs, subcategory)

                        if sub is not None:
                            subcategories_id.append(sub["id"])

            elif len(subcategories) == 1:
                options["subcategory"] = subcategories[0]


        def mismatch(name_a, name_b, limit=0.5):
            return SequenceMatcher(None, name_a, name_b).ratio() < limit

        for torrent in self.iter_torrents(options):

            if len(subcategories_id) > 1:

                if torrent.info.get("year") == year:
                    subcategories_id = [torrent.type]

                    bad = {t for t in torrents if t.type != torrent.type}
                    max_torrents += len([t for t in bad if t.size <= max_size])

                    torrents = deque(set(torrents) - bad, maxlen=max_torrents)

            elif subcategories_id:
                if torrent.type not in subcategories_id:
                    continue
            
            if abs(torrent.info.get("year", year) - year) > 1:
                print(torrent.name, torrent.info.get("year", 0), "outdated")
                continue

            torrent_name = torrent.info.get("title").lower()

            if name and torrent_name and mismatch(torrent_name, name, 0.8):
                if alt_name:
                    if mismatch(torrent_name, alt_name, 0.8):
                        print(torrent.info, "rejected", alt_name)
                        continue
                else:
                    print(torrent.info, "rejected", name)
                    continue

            if torrent.info.get("sbs"):
                print(torrent.info, "is 3d")
                continue

            torrents.append(torrent)

            if torrent.size <= max_size:
                if max_torrents > 0:
                    max_torrents -= 1
                else:
                    break

        return list(torrents)


    def iter_content(self, query):

        if self.scraper is None:
            return

        options = convert_options(query)

        orig_title  = query.get("original_title", "")
        title       = query.get("title","")
        date        = query.get("date", "")

        orig_title  = clean_title(orig_title)
        title       = clean_title(title)

        year = date.split("-")[0]
        if year.isdigit():
            year = int(year)
        else:
            year = None

        options.update(name=title)
    
        max_size = to_bytes(query.get("size", "2Go"))
    
        print("start scraping")

        torrents = self.search_torrents(options, max_size, year, orig_title)

        if self.config["search_original_title"] and orig_title:
            if not orig_title == title:
                options.update(name=orig_title)
                torrents.extend(self.search_torrents(options, max_size, year, title))

        max_torrents = self.get_max_torrent(options)
        torrents = select_best_torrents(max_size, torrents, max_torrents)

        for torrent in torrents:
            print(torrent.name)
            yield convert_to_content(torrent)

        print("finished scraping")


    def exchange_url(self, url):

        self.tracker_url = url
        return self.announce_url


    def spoof_torrent(self, id):

        if self.scraper is None:
            return

        torrent = self.scraper.download_torrent(id)

        if torrent is not None:
            try:
                data = bencode.decode(torrent)
            except bencode.BencodeDecodeError as e:
                logger.error(e)
            else:
                if not self.tracker_url:
                    from . import tracker
                    self.tracker_url = tracker.exchange_url(self.announce_url)

                data["announce"] = self.tracker_url
                return bencode.encode(data)
        

    @route("/torrent/<int:id>")
    def download_torrent(self, id):

        torrent = self.spoof_torrent(id)

        if torrent is not None:
            return Response(torrent, mimetype="application/x-bittorrent")
        
        return abort(status.INTERNAL_SERVER_ERROR)




    # def iter_content(self, query):

    #     torrent = Torrent()
    #     torrent.name = "a torrent"
    #     torrent.size = 1830000000
    #     torrent.url = "https://www.oxtorrent.pw/get_torrents/3ABA3F4F58709F1354E6E8CBCB711072975839EB"
    
    #     yield convert_to_content(torrent)