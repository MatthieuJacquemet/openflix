
patterns = [
    ('season', r'(s(?:(?:ea|ai)son[ \.\-_]*)?([0-9]{1,2}))[ex]?'),
    ('episode', r'((?:[eé]pisode[ \.\-_]+|[ex])([0-9]{1,2})(?:[^0-9]|$))'),
    ('year', r'([\[\(]?((?:19[0-9]|20[0-2])[0-9])[\]\)]?)'),
    ('resolution', r'([0-9]{3,4}p|4k(?:light)?|u(?:ltra)?hd)'),
    ('quality', (r'((?:PPV\.)?[HP]DTV|(?:HD)?CAM|B[DR]Rip|TS|(?:PPV '
                 r')?WEB-?DL(?: DVDRip)?|HDRip|DVDRip|DVDR'
                 r'IP|CamRip|W[EB]BRip|Blu-?Ray|DvDScr|hdtv)')),
    ('codec', r'(xvid|[hx]\.?26[45]|hevc|avc)'),
    ('audio', (r'((?:MP3|DD5\.?1|Dual[\- ]Audio|LiNE|H?DTS|AAC(?:\.?2\.0)'
               r'?|E?AC3)(?:[ \.\-_]*[75]\.1)?)')),
    ('group', r'(- ?([^-]+(?:-={[^-]+-?$)?))$'),
    ('region', r'R[0-9]'),
    ('extended', r'(EXTENDED(:?.CUT)?)'),
    ('hardcoded', r'HC'),
    ('proper', r'PROPER'),
    ('repack', r'REPACK'),
    ('container', r'(MKV|AVI)'),
    ('widescreen', r'WS'),
    ('hdr', r'(HDR|10[ \.\-]*bits?)'),
    ('website', r'^(\[ ?([^\]]+?) ?\])'),
    ('language', (r'(VF[FQ2I]?|(?:True[ \.\-]?)?French|'
                r'VO(?:ST(?:FR)?)?|Fr(?:ançais)?|MULTi)')),
    ('sbs', r'(?:Half-)?SBS|3D'),
]

types = {
    'season': 'integer',
    'episode': 'integer',
    'year': 'integer',
    'extended': 'boolean',
    'hardcoded': 'boolean',
    'proper': 'boolean',
    'repack': 'boolean',
    'widescreen': 'boolean',
    'hdr': 'boolean'
}
