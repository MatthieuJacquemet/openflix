from flask import abort, Response, request, url_for
from http import HTTPStatus as status
from openflix.interface import Interface
from openflix.openflix_utils import route
import logging
import requests
import bencode

from urllib import parse

logger = logging.getLogger(__name__)


def parse_query(query):

    return {k:v for k, _, v in map(lambda x : x.partition("="),
                                    query.split("&"))}
            

def unparse_query(data):

    return "&".join(f"{k}={v}" for k,v in data.items())


class YGG_Tracker(Interface):

    def __init__(self, app=None):
        super().__init__("ygg_tracker")

        self.app = app
        self.announce_url = ""
        self.tracker_url = ""

        if self.app is not None:
            self.init_app(app)


    def init_app(self, app):
        self.app = app
        self.load_config()

        with app.app_context():
            self.tracker_url = url_for("ygg_tracker.announce", _external=True)


    def exchange_url(self, url):
        
        self.announce_url = url
        return self.tracker_url


    def load_config(self):

        self.config = self.app.config.get_namespace("TRACKER_")


    def get_ygg_tracker(self):

        if not self.announce_url:
            from . import ygg
            self.announce_url = ygg.exchange_url(self.tracker_url)


    @route("/announce")
    def announce(self):

        self.get_ygg_tracker()
    
        if self.announce_url:
            
            original_url = parse.urlsplit(request.url)
            query_string = parse_query(original_url.query)

            headers = dict(request.headers)
            headers.pop("Host", None)

            if self.config["hide_complete"]:
                query_string["downloaded"] = 0

            new_query = unparse_query(query_string) #parse.urlencode(query_string,encoding="utf-8")
            url = parse.urlsplit(self.announce_url)
            url = url._replace(query=new_query)

            if query_string.get("event") == "started":
                max_retry = self.config["max_retry"]
                timeout = self.config["timeout"]
            else:
                max_retry = 1
                timeout = None

            while not max_retry == 0: 
                max_retry -= 1

                try:
                    resp = requests.get(
                        url=parse.urlunsplit(url),
                        headers=headers,
                        cookies=request.cookies,
                        timeout=timeout
                    )
                    
                except requests.Timeout:
                    continue
                except Exception as e:
                    logger.error(e)
                else:
                    try:
                        data = self.spoof_interval(resp.content)
                    except bencode.BencodeDecodeError:
                        data = resp.content

                    return (data, resp.status_code, resp.raw.headers)
            else:
                logger.error("maximum announce entempt exceeded")

        return abort(status.INTERNAL_SERVER_ERROR)


    def spoof_interval(self, data):

        interval = self.config["interval"]

        if interval is not None:
            data = bencode.decode(data)
            data["interval"] = interval
            data["min interval"] = interval
            data = bencode.encode(data)
        
        return data

    # self.session = requests.Session()
    # app.add_url_rule(view_func=self.announce, **self.filter_options())

    # def hide_download(self, query_string):

    #     params = query_string.split("&")

    #     for i, param in enumerate(params):
    #         key, _, _ = param.partition("=")
    #         if key == "downloaded":
    #             params[i] = "downloaded=0"
    #             break


    # def get_url(self):

    #     domain = self.config['subdomain'] + "." + app.config["SERVER_NAME"]
    #     return domain + self.config['rule']

    #     return "&".join(params)