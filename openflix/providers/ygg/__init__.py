from openflix import app, app
from .ygg import YGG_Provider
from .tracker import YGG_Tracker

app.load_config("ygg.default_settings")

ygg     = YGG_Provider(app)
tracker = YGG_Tracker(app)

def query_content(query):
    return list(ygg.iter_content(query))
