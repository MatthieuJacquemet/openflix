import os
import sys
import importlib
import logging
import ctypes
import threading

from http import HTTPStatus as status
from functools import wraps
from flask import jsonify, request, Response
from queue import Queue, Empty

from .openflix_utils import route
from .interface import Interface

from pyutils.common import get_err
from sqlalchemy.types import ARRAY

logger = logging.getLogger(__name__)


class ProviderHost(threading.Thread):

    def __init__(self, name, package, queue):

        super().__init__()

        self._query = None
        self._handle = None
        self._event = threading.Event()

        self.name = name
        self.queue = queue
        self.package = package
        self.daemon = True

        try:
            self._handle = importlib.import_module(name, package)
        except Exception as err:
            logger.error(err)


    def query_content(self, query):

        self._query = query
        self._event.set()


    def run(self):

        try:
            content = self._handle.query_content(self._query)
            self.queue.put(content)
        except Exception as e:
            logger.warn(e)


    def get_id(self): 
  
        thread_id = getattr(self, "_thread_id", None)
        if thread_id is not None:
            return thread_id

        for id, thread in threading._active.items(): 
            if thread is self: 
                return id
   

    def force_stop(self):

        thread_id = self.get_id() 
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 
              ctypes.py_object(SystemExit)) 
        if res > 1: 
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)



class ContentAPI(Interface):

    _providers = dict()
    _contents = dict()

    def __init__(self, app=None):

        self.app = app

        if self.app is not None:
            self.init_app(app)


    def init_app(self, app):

        self.app = app
        self.load_config()
        self.load_providers()

        if self.config["authentification"]:
            auth = {"content": None}
        else:
            auth = dict()

        super().__init__("media", auth=auth)


    def load_config(self):

        self.config = self.app.config.get_namespace("CONTENT_")


    def load_providers(self):

        self._providers = list()

        directory   = self.config["providers_dir"]
        preferred   = self.config["pref_provider"]
        extra       = self.config["extra_providers"]

        try:
            sys.path.insert(0, directory)

            for entity in os.listdir(directory) + extra:

                name, *_ = os.path.splitext(entity)
                try:
                    provider = importlib.import_module(name)
                except Exception as e:
                    logger.error(f"failled to load provider {name}: {e}")
                else:
                    if name == preferred:
                        self._providers.insert(0, provider)
                    else:
                        self._providers.append(provider)

            sys.path.remove(directory)

        except OSError as e:
            logger.error(e)

    
    def dispatch_query(self, query):

        content = list()
        print("providers", self._providers)
        for provider in self._providers:
            print("try provider ", provider)
        # try:
            content = provider.query_content(query)
        # except Exception as e:
        #     logger.error(e)
        # else:
            if not content:
                continue
        
        return content


    def stop_providers(self):

        for provider in self._providers:
            provider.force_stop()


    @route("/content", auth="content")
    def get_content(self):
        print("getting content", request.args)
        contents = self.dispatch_query(request.args)

        return jsonify({"results":contents}), status.OK


