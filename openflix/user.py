from . import db, app
from .openflix_utils import commit, sqla_to_object
import logging
import secrets
from werkzeug.security import generate_password_hash

class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(32), nullable=False)
    lastname = db.Column(db.String(32), nullable=False)
    email = db.Column(db.String(64), nullable=False, unique=True)
    password_hash = db.Column(db.String(128), nullable=False)
    token = db.Column(db.String(64), nullable=False, unique=True)
    role = db.Column(db.String(32), nullable=False)
    active = db.Column(db.Boolean)


def add_user(firstname, lastname, email, password, active=True, role="user"):

    token=secrets.token_hex(32)
    password_hash = generate_password_hash(password)

    user = User(firstname=firstname, lastname=lastname, email=email,
                password_hash=password_hash, token=token, role=role, 
                active=active)

    obj = sqla_to_object(user)

    db.session.add(user)
    db.session.commit()

    return obj


def del_user(email):
    
    user = get_user(email)
    if user:
        db.session.delete(user)
        db.session.commit()


def get_users_by(**kwargs) -> User:

    return User.query.filter_by(**kwargs)
