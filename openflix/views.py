from . import app
from flask import render_template, send_from_directory, request, jsonify, abort
from http import HTTPStatus as status
# from .user import add_user, get_user
from .token import add_token
from .openflix_utils import admin


# @app.errorhandler(404)
# def not_found(error):
#     return jsonify({'error':'not found'}), 404

# @app.errorhandler(401)
# def not_ffff(error):
#     return jsonify({'error':'not found'}), 401

# @app.errorhandler(403)
# def not_nnnn(error):
#     return jsonify({'error':'not found'}), 403

@app.route("/")
def index():
    return render_template("index.html", debug=app.debug)


@app.route("/movie")
def movie():
    return render_template("movie.html", debug=app.debug)


@app.route("/show")
def show():
    return render_template("show.html", debug=app.debug)


@app.route("/js/<path:path>")
def send_js(path):
    return send_from_directory("js", path)


# @app.route("/user")
# @admin
# def user():

#     username = request.header.get("username")
#     password_hash = request.headers.get("password_hash")
#     admin = request.headers.get("admin", False)
#     token = request.headers.get("session_token", False)

#     if request.method == "GET":
#         if username:
#             user = get_user(username)
#             ret = jsonify({ "password_hash":user.password_hash,
#                             "admin":user.admin,
#                             "session_token":user.token})

#             return ret, status.OK    

#     elif username and password_hash:

#         if request.method == "POST":
#             user = get_user(username)
#             user.set_password_hash(password_hash)
#             if admin is not None:
#                 user.set_admin(admin)
#             if token is not None:
#                 user.set_token(token)
#             return jsonify({}), status.OK

#         elif request.method == "PUT":
#             add_user(username, password_hash, admin, token)
#             return jsonify({}), status.OK

#     return abort(status.BAD_REQUEST)


# @app.route("/token")
# @admin
# def token():

#     name = request.header.get("name")
#     token = request.headers.get("value")

#     if request.method == "GET":
#         if name:
#             obj = get_token(name)
#             return jsonify({"token":obj.token}), status.OK    

#     elif name and token:

#         if request.method == "POST":
#             obj = get_token(name)
#             obj.set_token(token)
#             return jsonify({}), status.OK

#         elif request.method == "PUT":
#             add_token(name, token)
#             return jsonify({}), status.OK

#     return abort(status.BAD_REQUEST)

# @socket.on("connect", namespace="/test")
# def test_connect():
#     print("Client connected")


# @socket.on("disconnect", namespace="/test")
# def test_disconnect():
#     print("Client disconnected")
