import os
import logging
import time
import sys

from queue import Queue, Empty
from http import HTTPStatus as status
from string import hexdigits
from .openflix_utils import convert_config

from pyutils.initialzation import parse_args
from pyutils.common import get_err

from flask import request, Flask


logger = logging.getLogger(__name__)


class OpenFlix(Flask):

    def __init__(self):
        super().__init__("openflix")

        self.options = parse_args(sys.argv)
        self.load_config()
        self.init_logger()


    def init_logger(self):

        if self.config["LOG_FILE"] is not None:

            logging.basicConfig(
                filename=self.config["LOG_FILE"],
                level=self.config["LOG_LEVEL"],
                format="%(asctime)s - %(message)s",
                datefmt="%m/%d/%Y %I:%M:%S %p"
            )


    def run(self, *args, **kwargs):

        from . import socket
        sio_options = self.config.get_namespace("SOCKETIO_")
        socket.init_app(self, **convert_config(sio_options))

        kwargs.setdefault("host", self.config["HOST"])
        kwargs.setdefault("port", self.config["PORT"])

        super().run(*args, **kwargs)


    def load_config(self, defaults="openflix.default_settings"):

        self.config.from_object(defaults)

        if "config" in self.options:
            self.config.from_pyfile(self.options["config"])

        elif "OPENFLIX_SETTINGS" in os.environ:
            self.config.from_envvar('OPENFLIX_SETTINGS')

