from . import db, app
from .openflix_utils import commit
import uuid
import secrets

class Token(db.Model):

    id      = db.Column(db.Integer, primary_key=True)
    value   = db.Column(db.String(64), nullable=False)


def add_token(value):

    with app.app_context():
        token = Token(value=value)
        db.session.add(token)
        db.session.commit()


def new_token():

    token = secrets.token_hex(32)
    add_token(token)
    return token


def remove_token(value) -> Token:

    with app.app_context():
        tokens = Token.query.filter_by(value=value).all()
        for token in tokens:
            db.session.remove(token)
            db.session.commit()

def has_token(value):

    with app.app_context():
        if Token.query.filter_by(value=value).first() is None:
            return False
        return True