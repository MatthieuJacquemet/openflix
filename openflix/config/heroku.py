from openflix.openflix_utils import module_path, get_int
import os

LOG_LEVEL                   = "ERROR"
TESTING                     = False
DEBUG                       = False

SQLALCHEMY_DATABASE_URI     = os.environ.get("DATABASE_URL")

LANGUAGES                   = ("en","fr")
HOST                        = "0.0.0.0"
PORT                        = get_int(os.environ.get("PORT",""))

CONTENT_LANGUAGES           = ("fr-FR", "en-US")

ACCESS_REQUIRE_ACTIVATION   = True

CONTENT_AUTHENTIFICATION    = True

SOCKETIO_HOST               = HOST

YGG_USERNAME                = os.environ.get("YGG_USERNAME")
YGG_PASSWORD                = os.environ.get("YGG_PASSWORD")