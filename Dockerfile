# Copyright (C) 2022 Matthieu Jacquemet
# 
# This file is part of openflix.
# 
# openflix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# openflix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with openflix.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:latest

# install dependencies
ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install xvfb python3 python3-pip
RUN pip3-install -r requirements.txt
RUN playwright install chrome && playwright install-deps

# add app user 
RUN useradd -ms /bin/bash app
USER app
WORKDIR /home/app