from setuptools import setup

setup(
    name='openflix',
    packages=['openflix'],
    include_package_data=True,
    install_requires=[
        'flask',"flask_restful","flask_sqlalchemy","flask_cors","flask_socketio"
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Web Development :: Web Site",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)